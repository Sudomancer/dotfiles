; Polybar config: bottom bar.

[colours]
background = ${xrdb:color0}
light-background = ${xrdb:color8}
foreground = ${xrdb:color7}

primary = ${xrdb:color1}
secondary = ${xrdb:color6}

alert = #bd2c40

[bar/D1]
monitor = DVI-D-1
width = 100%
height = 28
bottom = true

background = ${colours.background}
foreground = ${colours.foreground}

line-size = 1
line-color = ${colours.primary}

module-margin-left = 2
margin-left = 2
module-margin-right = 2
margin-right = 2

fixed-center = true
radius = 5.0

	font-0 = InconsolataLGC Nerd Font Mono:size=8;1
font-1 = NotoSansSymbols-Regular:size=10;2
font-2 = NotoSansSymbols2-Regular:size=10;2
font-3 = NotoSansCJK-Regular:size=10

modules-left = spacer wired-network ping user spacer openweathermap 
modules-center = spotify
modules-right = cpu memory filesystem spacer

override-redirect = false

pseudo-transparency = true

cursor-click = pointer
cursor-scroll = ns-resize

[module/cpu]
type = internal/cpu
interval = 0.5
format = <label> <ramp-coreload>
label = CPU:

ramp-coreload-0 = ▁
ramp-coreload-0-font = 0
ramp-coreload-0-foreground = ${colours.secondary}
ramp-coreload-1 = ▂
ramp-coreload-1-font = 0
ramp-coreload-1-foreground = ${colours.secondary}
ramp-coreload-2 = ▃
ramp-coreload-2-font = 0
ramp-coreload-2-foreground = ${colours.secondary}
ramp-coreload-3 = ▄
ramp-coreload-3-font = 0
ramp-coreload-3-foreground = ${colours.secondary}
ramp-coreload-4 = ▅
ramp-coreload-4-font = 0
ramp-coreload-4-foreground = #fba922
ramp-coreload-5 = ▆
ramp-coreload-5-font = 0
ramp-coreload-5-foreground = #fba922
ramp-coreload-6 = ▇
ramp-coreload-6-font = 0
ramp-coreload-6-foreground = #ff5555
ramp-coreload-7 = █
ramp-coreload-7-font = 0
ramp-coreload-7-foreground = #ff5555

[module/memory]
type = internal/memory
format = <label> <bar-used>
label = RAM: 

bar-used-width = 30
bar-used-foreground-0 = ${colours.secondary}
bar-used-foreground-1 = ${colours.secondary}
bar-used-foreground-2 = #fba922
bar-used-foreground-3 = #ff5555
bar-used-indicator = |
bar-used-indicator-font = 0
bar-used-indicator-foreground = #ff
bar-used-fill = ─
bar-used-fill-font = 0
bar-used-empty = ─
bar-used-empty-font = 0
bar-used-empty-foreground = #666666

[module/filesystem]
type = internal/fs
mount-0 = /
interval = 5

[module/spotify]
type = custom/script
interval = 1
label-font = 0
format-prefix = "🎜 "
format = <label>
exec = python ~/.config/polybar/spotify.py -f '{artist} - {song}'

[module/wired-network]
type = internal/network
interface = enp4s0
interval = 3.0
label-font = 0
;label-connected = 🌎 %{T3}%local_ip%%{T-}
label-connected = 🖧 %local_ip%
label-disconnected-foreground = #66

[module/openweathermap]
type = custom/script
exec = ~/.config/polybar/openweathermap.sh
format-prefix = "🌫 "
interval = 600

[module/ping]
type = custom/script
exec = ~/.config/polybar/ping.sh
interval = 5

[module/spacer]
type = custom/text
content = " "

[module/user]
type = custom/script
format-prefix = "🚹 "
exec = whoami

[global/wm]
margin-bottom = 5
margin-top = 5
; vim:ft=dosini
