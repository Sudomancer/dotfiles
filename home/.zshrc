# Variables and options:
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob
setopt appendhistory
bindkey -v
zstyle :compinstall filename '/home/pianoman/.zshrc'
zstyle ':completion:*' rehash true

autoload -Uz compinit
compinit

ttyctl -f


# Aliases:
alias suspend='systemctl suspend'
alias ls="ls -A --color"
alias e="$EDITOR"


# Exports:
export EDITOR="nvim"
export QT_QPA_PLATFORMTHEME="qt5ct"


# Sources:
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/doc/pkgfile/command-not-found.zsh

# Prompt:
source ~/.prompt-emdash.zsh

eval $(thefuck --alias)
